{% from "prometheus/map.jinja" import prometheus with context %}

include:
  - prometheus.user

postgres_exporter_tarball:
  archive.extracted:
    - name: {{ prometheus.exporter.postgres.install_dir }}
    - source: {{ prometheus.exporter.postgres.source }}
    - source_hash: {{ prometheus.exporter.postgres.source_hash }}
    - user: {{ prometheus.user }}
    - group: {{ prometheus.group }}
    - archive_format: tar
    - if_missing: {{ prometheus.exporter.postgres.version_path }}

{#
postgres_exporter_bin_link:
  file.symlink:
    - name: /usr/bin/postgres_exporter
    - target: {{ prometheus.exporter.postgres.version_path }}/postgres_exporter
    - require:
      - archive: postgres_exporter_tarball
#}

postgres_exporter_defaults:
  file.managed:
    - name: /etc/default/postgres_exporter
    - source: salt://prometheus/files/default-postgres_exporter.jinja
    - template: jinja
    - defaults:
        data_source_name: {{ prometheus.exporter.postgres.args.get('data_source_name', "") }}

postgres_exporter_service_unit:
  file.managed:
{%- if grains.get('init') == 'systemd' %}
    - name: /etc/systemd/system/postgres_exporter.service
    - source: salt://prometheus/files/exporter.systemd.jinja
{%- elif grains.get('init') == 'upstart' %}
    - name: /etc/init/postgres_exporter.conf
    - source: salt://prometheus/files/exporter.upstart.jinja
{%- endif %}
    - template: jinja
    - defaults:
        svc: postgres_exporter
        user: {{ prometheus.exporter.postgres.args.get('user', prometheus.user) }}
        group: {{ prometheus.exporter.postgres.args.get('group', prometheus.group) }}
        bin_path: {{ prometheus.exporter.postgres.version_path }}
        args: {{ prometheus.exporter.postgres.args.service_args }}
        url: {{ prometheus.exporter.postgres.args.url }}
    - require_in:
      - file: postgres_exporter_service

postgres_exporter_service:
  service.running:
    - name: postgres_exporter
    - enable: True
    - reload: False
    - watch:
      - file: postgres_exporter_service_unit
      - file: postgres_exporter_defaults
      {# - file: postgres_exporter_bin_link #}
